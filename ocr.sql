/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.3.251
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : 192.168.3.251
 Source Database       : ocr

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : utf-8

 Date: 04/12/2017 15:25:30 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `ocr_baidu_apps`
-- ----------------------------
DROP TABLE IF EXISTS `ocr_baidu_apps`;
CREATE TABLE `ocr_baidu_apps` (
  `app_id` bigint(64) NOT NULL,
  `app_key` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `secret_key` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `expires_time` bigint(20) DEFAULT '0',
  `last_time` bigint(20) DEFAULT '0',
  `ocr_count` int(11) DEFAULT '0',
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
--  Table structure for `ocr_count`
-- ----------------------------
DROP TABLE IF EXISTS `ocr_count`;
CREATE TABLE `ocr_count` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '格式为appid_日期，如23423_20170210',
  `app_id` bigint(20) DEFAULT NULL,
  `ocr_count` int(11) DEFAULT '0',
  `ocr_date` int(11) DEFAULT NULL,
  `ocr_last_time` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
--  Table structure for `ocr_system`
-- ----------------------------
DROP TABLE IF EXISTS `ocr_system`;
CREATE TABLE `ocr_system` (
  `sysid` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '系统ID',
  `url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '系统数据上报地址',
  `sys_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '系统名称',
  PRIMARY KEY (`sysid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
--  Table structure for `ocr_task`
-- ----------------------------
DROP TABLE IF EXISTS `ocr_task`;
CREATE TABLE `ocr_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_hash` varchar(40) COLLATE utf8mb4_bin DEFAULT NULL,
  `mid` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `state` int(11) DEFAULT '0' COMMENT '0，等待本地ocr\r\n1，等待云端ocr\r\n2，等待上传\r\n3.  已经上传',
  `create_time` bigint(20) DEFAULT NULL,
  `word_count` int(11) DEFAULT NULL,
  `ocr_content` text COLLATE utf8mb4_bin COMMENT 'ocr原始json，用来在界面呈现',
  `ocr_flg` tinyint(4) DEFAULT '0' COMMENT '0,无数据，1, 有数据\r\n 2,出错',
  `sysid` varchar(128) COLLATE utf8mb4_bin DEFAULT '0' COMMENT 'local ocr识别出来的字数',
  `dispatch_time` bigint(20) DEFAULT '0',
  `base_hash` varchar(40) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `file_hash_index` (`file_hash`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=62376 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

SET FOREIGN_KEY_CHECKS = 1;
