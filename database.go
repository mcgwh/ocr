package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	"github.com/cihub/seelog"
	_ "github.com/go-sql-driver/mysql" // import your used driver
)

type Task struct {
	Id           int64 `orm:"pk"`
	FileHash     string
	Mid          string
	State        int
	CreateTime   int64
	WordCount    int
	OcrContent   string
	OcrFlg       int
	DispatchTime int64
	BaseHash     string

	Sysid string
}

type System struct {
	Sysid   string `orm:"pk"`
	Url     string
	SysName string
}

type BaiduApps struct {
	AppId       int64 `orm:"pk"`
	AppKey      string
	SecretKey   string
	AccessToken string
	ExpiresTime int64
	LastTime    int64
	OcrCount    int
}

var MinCharCount int

func InitOrm() {
	orm.RegisterModelWithPrefix("ocr_", new(System), new(BaiduApps), new(Task))
	err := orm.RegisterDataBase("default", "mysql", GetConfigString("mysql", "MySQLConn", ""), 3, 6)

	MinCharCount = GetConfigInt("ocr", "MinCharCount", 30)

	if err != nil {
		seelog.Error("InitOrm failed, ", err)
		seelog.Flush()
		panic(err)
	}

	seelog.Info("InitOrm 成功...")
}

func GetSystem(id string) *System {
	var item System

	o := orm.NewOrm()
	err := o.QueryTable("ocr_system").Filter("sysid", id).One(&item)
	if err != nil {
		seelog.Error("GetSystem失败： ", err)
		return nil
	}

	return &item
}

func SaveTask(task *Task) error {
	o := orm.NewOrm()

	if id, err := o.Insert(task); err != nil {
		seelog.Errorf("SaveTask 失败： %s", err.Error())
		return err
	} else {
		task.Id = id
	}

	return nil
}

func GetTasks(max, state int) []*Task {
	o := orm.NewOrm()

	var tasks []*Task

	if _, err := o.QueryTable("ocr_task").Filter("state", state).Limit(max).All(&tasks); err != nil {
		seelog.Errorf("GetTasks失败： %s", err.Error())
		return nil
	}

	return tasks
}

func GetLocalOcrTasks(max int) []*Task {
	o := orm.NewOrm()

	var tasks []*Task

	if _, err := o.QueryTable("ocr_task").Filter("state", 0).Filter("dispatch_time__lte", time.Now().Unix()-3600).Limit(max).All(&tasks); err != nil {
		seelog.Errorf("GetTasks失败： %s", err.Error())
		return nil
	}

	return tasks
}

func UpdateTasks(task *Task) error {
	o := orm.NewOrm()

	_, err := o.Update(task)
	if err != nil {
		seelog.Error("UpdateTasks 失败", err)
		return err
	}

	return nil
}

func UpdateDispathTime(tasks []*Task) error {
	if len(tasks) == 0 {
		return nil
	}

	ids := "("

	for _, v := range tasks {
		ids = ids + strconv.FormatInt(v.Id, 10) + ","
	}

	ids = strings.TrimRight(ids, ",") + ")"

	o := orm.NewOrm()
	sql := "update ocr_task set dispatch_time = ? where id in" + ids

	_, err := o.Raw(sql, time.Now().Unix()).Exec()
	if err != nil {
		seelog.Error("UpdateDispathTime 失败: ", err.Error())
		return err
	}

	return nil
}

func SaveLocalOcrResult(id int64, data string) error {
	data = strings.Replace(data, " ", "", -1)
	data = strings.Replace(data, "\t", "", -1)
	data = strings.Replace(data, "\n", "", -1)
	data = strings.Replace(data, "\f", "", -1)
	data = strings.Replace(data, "\r", "", -1)

	seelog.Debug("LocalOcrData 去除空白: ", data)

	n := len(data)

	state := 1
	if n < MinCharCount {
		state = 2
	}

	sql := "update ocr_task set state = ?, word_count = ? where id = ?"

	o := orm.NewOrm()
	if _, err := o.Raw(sql, state, n, id).Exec(); err != nil {
		seelog.Error("SaveLocalOcrResult failed: ", err)
		return err
	}

	return nil
}

func SaveOcrCount(appid int64, count, date int) error {
	id := strconv.FormatInt(appid, 10) + "_" + strconv.FormatInt(int64(date), 10)

	seelog.Infof("OcrCount 更新， appid = %d date = %d ocr_count = %d", appid, date, count)

	o := orm.NewOrm()
	sql := "replace into ocr_count (id, app_id, ocr_count, ocr_date) values(?,?,?,?)"

	if _, err := o.Raw(sql, id, appid, count, date).Exec(); err != nil {
		seelog.Error("SaveOcrCount 失败： ", err)
		return err
	}

	return nil
}

func UpdateBaiduApp(app *BaiduApps) error {
	o := orm.NewOrm()
	if _, err := o.Update(app); err != nil {
		seelog.Error("UpdateBaiduApp 失败：", err)
		return err
	}

	return nil
}

func GetBaiduApps() []BaiduApps {
	o := orm.NewOrm()

	var apps []BaiduApps

	sql := "SELECT ocr_baidu_apps.app_id as app_id,app_key,secret_key,access_token, expires_time,  IFNULL( ocr_count.ocr_count,0) as ocr_count from ocr_baidu_apps left JOIN ocr_count on (ocr_baidu_apps.app_id = ocr_count.app_id and ocr_count.ocr_date = ? ) where IFNULL(ocr_count.ocr_count,0)<490 ORDER BY ocr_baidu_apps.last_time asc,IFNULL(ocr_count.ocr_count,0) desc"

	n, err := o.Raw(sql, GetDate()).QueryRows(&apps)
	if err != nil {
		seelog.Error("GetBaiduApps 失败：", err)
		return nil
	}

	seelog.Infof("获取到%d个baidu app", n)
	seelog.Info(apps)

	return apps
}

func DeleteTasks(ids string) error {
	if ids == "" {
		return nil
	}

	sql := fmt.Sprintf("delete from ocr_task where id in (%s)", ids)
	o := orm.NewOrm()

	_, err := o.Raw(sql).Exec()
	if err != nil {
		seelog.Error("DeleteTasks 失败: ", err)
		return err
	}

	return nil
}

func DeleteTask(task *Task) error {
	o := orm.NewOrm()

	_, err := o.Delete(task)
	if err != nil {
		seelog.Error("DeleteTask 失败: ", err)
		return err
	}

	return nil
}
