package main

import "github.com/syndtr/goleveldb/leveldb"

var SysCache *leveldb.DB

func InitCache() error {
	var err error

	SysCache, err = leveldb.OpenFile(GetConfigString("cache", "cacheDir", "cache.dat"), nil)
	if err != nil {
		return err
	}

	return nil
}

func CloseCache() {
	if SysCache != nil {
		SysCache.Close()
	}
}

func CachePut(key, value []byte) error {
	return SysCache.Put(key, value, nil)
}

func CacheGet(key []byte) ([]byte, error) {
	return SysCache.Get(key, nil)
}
