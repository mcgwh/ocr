package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/cihub/seelog"
)

type WordsResult struct {
	Words string `json:"words"`
}

type OcrResult struct {
	Count int           `json:"words_result_num"`
	Words []WordsResult `json:"words_result"`
}

type ErrorResponse struct {
	Code int    `json:"error_code"`
	Msg  string `json:"error_msg"`
}

type Token struct {
	AccessToken string `json:"access_token"`
	ExpiresTime int64  `json:"expires_in"`
}

type BaiduOcr struct {
	client *HttpClientEx
	app    *BaiduApps
	date   int
	flg    int
}

var InvalidImage string = "Invalid Image"

func (this *BaiduOcr) InitOcr() {
	this.client = &HttpClientEx{}

	this.ChangeApp()

	this.date = GetDate()
	this.flg = 0
}

func GetDate() int {
	t := time.Now().Unix()
	s := time.Unix(t, 0).Format("20060102")

	val, _ := strconv.ParseInt(s, 10, 64)

	return int(val)
}

func (this *BaiduOcr) UpdateDate() {
	if this.app == nil {
		return
	}

	date := GetDate()
	if date != this.date {
		this.app.OcrCount = 0
		this.date = date

		SaveOcrCount(this.app.AppId, this.app.OcrCount, this.date)
	}
}

func (this *BaiduOcr) UpdateAccessToken() error {
	if this.app.ExpiresTime > time.Now().Unix() {
		seelog.Info("AccessToken 未过期，无需更新...")
		return nil
	}

	tokenUrl := "https://aip.baidubce.com/oauth/2.0/token?" +
		"grant_type=client_credentials&" +
		"client_id=%s&" +
		"client_secret=%s"

	tokenUrl = fmt.Sprintf(tokenUrl, url.QueryEscape(strings.TrimSpace(this.app.AppKey)), url.QueryEscape(strings.TrimSpace(this.app.SecretKey)))

	seelog.Debug("Update AccessToken: ", tokenUrl)

	data, err := this.client.HttpGet(tokenUrl)
	if err != nil {
		seelog.Error("UpdateAccessToken 失败： ", err)
		return err
	}

	seelog.Info("AccessToken: ", string(data))

	now := time.Now().Unix()

	token := &Token{}
	err = json.Unmarshal(data, token)
	if err != nil {
		seelog.Error("UpdateAccessToken 失败： ", err)
		this.app.LastTime = now

		UpdateBaiduApp(this.app)
		this.flg = 1

		return err
	}

	if token.AccessToken == "" {
		seelog.Error("UpdateAccessToken 失败： AccessToken为空")
		this.app.LastTime = now
		UpdateBaiduApp(this.app)
		this.flg = 1

		return err
	}

	this.app.AccessToken = token.AccessToken
	this.app.ExpiresTime = now + token.ExpiresTime - int64(7200)

	UpdateBaiduApp(this.app)

	seelog.Info("AppInfo: ", this.app)

	return nil
}

func (this *BaiduOcr) ChangeApp() error {
	for {
		apps := GetBaiduApps()
		if len(apps) > 0 {
			this.app = &apps[0]
			this.date = GetDate()
			this.flg = 0

			seelog.Infof("取到app, id = %d date = %d ocr_count = %d", this.app.AppId, this.date, this.app.OcrCount)

			break
		}

		seelog.Error("未取到可用baidu app，稍后重试...")
		time.Sleep(60 * time.Second)
	}

	return nil
}

func (this *BaiduOcr) ProcessOcr(data []byte) (string, error) {
	for {
		result, err := this.ProcessImage(data)
		if err != nil {
			seelog.Errorf("ProcessImage 失败: %s", err.Error())

			if err.Error() == InvalidImage {
				return "", err
			}

			this.app.LastTime = time.Now().Unix()
			UpdateBaiduApp(this.app)

			seelog.Info("更换APP...")
			this.ChangeApp()

			time.Sleep(5 * time.Second)
		} else {
			this.UpdateDate()
			this.app.OcrCount = this.app.OcrCount + 1
			SaveOcrCount(this.app.AppId, this.app.OcrCount, this.date)

			return result, nil
		}
	}

	return "", nil
}

func (this *BaiduOcr) ProcessImage(data []byte) (string, error) {
	this.UpdateDate()
	if this.app.OcrCount >= 500 {
		this.ChangeApp()
	}

	for {
		err := this.UpdateAccessToken()
		if err != nil {
			if this.flg == 1 {
				seelog.Info("flg==1， 更换app...")
				this.ChangeApp()
			} else {
				seelog.Infof("可能是网络问题，UpdateAccessToken失败： %s", err.Error())
				time.Sleep(10 * time.Second)
			}
		}

		break
	}

	apiurl := "https://aip.baidubce.com/rest/2.0/ocr/v1/general?access_token=" + this.app.AccessToken
	body := "image=" + url.QueryEscape(base64.StdEncoding.EncodeToString(data))

	for {
		result, err := this.client.HttpPost(apiurl, body)
		if err != nil {
			seelog.Error("调用API失败，稍后重试...")

			time.Sleep(20 * time.Second)
		} else {
			seelog.Info("baidu ocr result: ", string(result))

			ErrorResult := &ErrorResponse{}

			err = json.Unmarshal(result, ErrorResult)
			if err == nil {
				if ErrorResult.Code >= 216200 && ErrorResult.Code <= 216202 {
					seelog.Errorf("baidu ocr错误， 图片可能存在问题: %d %s", ErrorResult.Code, ErrorResult.Msg)
					return "", errors.New(InvalidImage)
				}

				if ErrorResult.Code != 0 {
					seelog.Errorf("调用API失败，可能需要更换APP: %d %s", ErrorResult.Code, ErrorResult.Msg)
					return "", errors.New(string(result))
				}
			}

			rs := &OcrResult{}
			err = json.Unmarshal(result, rs)
			if err != nil {
				seelog.Errorf("解析OCR结果失败： %s", err.Error())
				return "", err
			}

			return StructToJson(rs), nil
		}
	}

	return "", nil
}
