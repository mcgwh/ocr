package main

import (
	"bytes"
	"compress/gzip"
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/cihub/seelog"
	"github.com/go-ini/ini"
	"github.com/satori/go.uuid"
)

var SysConfigDataMap map[string]string
var ConfigFlag int = 0
var SysConfigSeprator string = "__config__"

func CurDir() string {
	file, _ := exec.LookPath(os.Args[0])

	path, _ := filepath.Abs(file)
	return filepath.Dir(path) + string(os.PathSeparator)
}

func InitLog() error {
	logger, err := seelog.LoggerFromConfigAsFile(CurDir() + "log.xml")
	if err != nil {
		fmt.Println(err)
		return err
	}

	seelog.ReplaceLogger(logger)
	return nil
}

func InitSysConfig(filename string) {
	InitLog()

	cfg, err := ini.Load(CurDir() + filename)
	if err != nil {
		seelog.Errorf("加载配置文件%s失败： %s, panic", filename, err.Error())
		seelog.Flush()

		panic("加载配置文件失败")

		return
	}

	SysConfigDataMap = make(map[string]string, 100)

	sections := cfg.SectionStrings()

	for _, section := range sections {
		keys := cfg.Section(section).KeyStrings()

		for _, key := range keys {
			k := section + SysConfigSeprator + key
			v := cfg.Section(section).Key(key).String()

			SysConfigDataMap[k] = v
		}
	}

	ConfigFlag = 1

	seelog.Info("加载配置文件成功...")

}

func CheckConfigStatus() {
	if ConfigFlag == 0 {
		seelog.Info("请先调用InitSysConfig成功加载配置文件, panic...")
		seelog.Flush()

		panic("请先调用LoadConfig加载配置文件...")
	}
}

func GetData(section, key string) (string, bool) {
	k := section + SysConfigSeprator + key
	data, flg := SysConfigDataMap[k]

	return data, flg
}

func GetConfigInt64(section, key string, default_value int64) int64 {
	CheckConfigStatus()

	data, flg := GetData(section, key)
	if !flg {
		seelog.Warnf("未能查询到配置项: section = %s, key = %s, 返回默认值 %d", section, key, default_value)
		return default_value
	}

	val, err := strconv.ParseInt(data, 10, 64)
	if err != nil {
		seelog.Warnf("转换配置项至int64类型失败(%s): section = %s, key = %s, value = %s, 返回默认值 %d", err.Error(), section, key, data, default_value)

		return default_value
	}

	return val
}

func GetConfigInt(section, key string, default_value int) int {
	return int(GetConfigInt64(section, key, int64(default_value)))
}

func GetConfigString(section, key string, default_value string) string {
	CheckConfigStatus()

	data, flg := GetData(section, key)
	if !flg {
		seelog.Warnf("未能查询到配置项: section = %s, key = %s, 返回默认值 %s", section, key, default_value)

		return default_value
	}

	return data
}

func SysConfigToString() string {
	result := "SysConfig:\n"

	for k, v := range SysConfigDataMap {
		result = result + "\t[" + strings.Replace(k, SysConfigSeprator, "]\t", 1) + " = " + v + "\n"
	}

	return strings.TrimRight(result, "\n")
}

func ComputeSha1(data []byte) []byte {
	hash := sha1.New()

	hash.Write(data)
	return hash.Sum(nil)
}

func ComputeSha1ToHex(data []byte) string {
	return hex.EncodeToString(ComputeSha1(data))
}

func ComputeMd5(data []byte) []byte {
	hash := md5.New()

	hash.Write(data)
	return hash.Sum(nil)
}

func ComputeMd5ToHex(data []byte) string {
	return hex.EncodeToString(ComputeMd5(data))
}

func StructToJson(v interface{}) string {
	data, _ := json.Marshal(v)
	return string(data)
}

func GZipData(data []byte) []byte {
	var b bytes.Buffer
	w := gzip.NewWriter(&b)
	defer w.Close()

	w.Write(data)

	w.Flush()

	return b.Bytes()
}

func GUNZipData(data []byte) ([]byte, error) {
	var b bytes.Buffer

	b.Write(data)

	r, _ := gzip.NewReader(&b)
	defer r.Close()

	return ioutil.ReadAll(r)
}

func NewUUID() string {
	return uuid.NewV1().String()
}

func Exist(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil || os.IsExist(err)
}
