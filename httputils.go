package main

import (
	"errors"
	"strconv"
	"time"

	"github.com/cihub/seelog"
	"github.com/valyala/fasthttp"
)

type HttpClientEx struct {
	client fasthttp.Client

	UserAgent  string
	StatusCode int
	Timeout    int
}

func (this *HttpClientEx) HttpDoRequest(req *fasthttp.Request, resp *fasthttp.Response) ([]byte, error) {
	timeout := this.Timeout

	if timeout == 0 {
		timeout = 30
	}

	err := this.client.DoTimeout(req, resp, time.Duration(timeout)*time.Second)
	if err != nil {
		seelog.Errorf("下载页面%s 失败， %s", string(req.RequestURI()), err.Error())
		return nil, err
	}

	if resp.StatusCode() != 200 {
		seelog.Errorf("Http请求失败: status_code = %d", resp.StatusCode())
		return nil, errors.New("http_status_code = " + strconv.FormatInt(int64(resp.StatusCode()), 10))
	}

	encoding := string(resp.Header.Peek("Content-Encoding"))

	err = nil

	var data []byte
	if encoding == "gzip" {
		data, err = resp.BodyGunzip()
	} else if encoding == "deflate" {
		data, err = resp.BodyInflate()
	} else {
		data = resp.Body()
	}

	return data, err
}

func (this *HttpClientEx) HttpGet(url string) ([]byte, error) {
	this.StatusCode = 0

	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()

	if this.UserAgent == "" {
		req.Header.SetUserAgent("Chrome 55")

	} else {
		req.Header.SetUserAgent(this.UserAgent)
	}

	req.SetRequestURI(url)
	req.Header.Add("Accept", "*/*")
	req.Header.Add("Accept-Language", "zh-cn")
	req.Header.Add("Accept-Encoding", "gzip, deflate")
	req.Header.Add("Connection", "keep-alive")

	return this.HttpDoRequest(req, resp)
}

func (this *HttpClientEx) HttpPost(url string, data string) ([]byte, error) {
	this.StatusCode = 0

	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()

	if this.UserAgent == "" {
		req.Header.SetUserAgent("Chrome 55")
	} else {
		req.Header.SetUserAgent(this.UserAgent)
	}

	req.Header.SetMethod("POST")
	req.Header.SetContentType("application/x-www-form-urlencoded")

	req.SetRequestURI(url)

	req.Header.Add("Accept", "*/*")
	req.Header.Add("Accept-Language", "zh-cn")
	req.Header.Add("Accept-Encoding", "gzip, deflate")
	req.Header.Add("Connection", "keep-alive")

	req.SetBody([]byte(data))

	return this.HttpDoRequest(req, resp)
}

func (this *HttpClientEx) HttpUpload(url string, data []byte) ([]byte, error) {
	this.StatusCode = 0

	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()

	if this.UserAgent == "" {
		req.Header.SetUserAgent("Chrome 55")
	} else {
		req.Header.SetUserAgent(this.UserAgent)
	}

	req.Header.SetMethod("POST")
	req.Header.SetContentType("application/octet-stream")

	req.SetRequestURI(url)

	req.Header.Add("Accept", "*/*")
	req.Header.Add("Accept-Language", "zh-cn")
	req.Header.Add("Accept-Encoding", "gzip, deflate")
	req.Header.Add("Connection", "keep-alive")

	req.SetBody([]byte(data))

	return this.HttpDoRequest(req, resp)
}
