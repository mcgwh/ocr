package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/cihub/seelog"
)

var SessionKey string

type ResultEntry struct {
	Id      string
	Content string
}

type UploadResponse struct {
	Error int
	Msg   string
}

func Hash2File(checksum string) string {
	sep := string(os.PathSeparator)

	dir := ImageDir + sep + checksum[0:2] + sep + checksum[2:4] + sep + checksum[4:6]

	filename := dir + sep + checksum + ".jpg"

	return filename
}

func ReadDataFromFile(fileHash string) ([]byte, error) {
	filename := Hash2File(fileHash)
	return ioutil.ReadFile(filename)
}

func ProcessCloudOcr() {
	ocr := &BaiduOcr{}

	ocr.InitOcr()

	for {
		tasks := GetTasks(10, 1)

		n := len(tasks)

		seelog.Infof("取到%d个图片，需要baidu识别...", n)
		if n == 0 {
			time.Sleep(15 * time.Second)
			continue
		}

		for _, v := range tasks {
			//调用baidu OCR

			result, err := OcrImageByCache(v.BaseHash)
			if err == nil {
				seelog.Infof("从缓存中查询得到结果， %s : %s", v.FileHash, string(result))

				SaveBaiduOcrResult(v, string(result), false)
			} else {
				data, err := ReadDataFromFile(v.FileHash)
				if err != nil {
					seelog.Error("读取文件失败：", v.FileHash, " ", err)
					v.State = 2

					UpdateTasks(v)

					continue
				}

				bdResult, err := ocr.ProcessOcr(data)
				if err != nil {
					seelog.Errorf("百度OCR失败: %s", err.Error())

					v.OcrFlg = 2
					v.State = 2

					UpdateTasks(v)

					time.Sleep(10 * time.Second)
					continue
				}

				seelog.Infof("从baidu ocr得到结果， %s : %s", v.FileHash, bdResult)

				SaveBaiduOcrResult(v, bdResult, true)
				time.Sleep(10 * time.Second)

			}
		}

	}
}

func OcrImageByCache(FileHash string) ([]byte, error) {
	key := []byte("ocr_" + FileHash)

	return CacheGet(key)
}

func SaveBaiduOcrResult(task *Task, result string, saveCache bool) {
	if saveCache {
		key := []byte("ocr_" + task.BaseHash)

		CachePut(key, []byte(result))
	}

	task.OcrContent = result
	task.OcrFlg = 1
	task.State = 2

	UpdateTasks(task)

	filename := Hash2File(task.FileHash)
	os.Remove(filename)
}

func UploadData(task *Task, client *HttpClientEx) {
	sysinfo := GetSystem(task.Sysid)
	if sysinfo == nil {
		seelog.Errorf("未能查到sysid=%s对应的系统地址，不上报...", task.Sysid)
		return
	}

	ts := strconv.FormatInt(time.Now().Unix(), 10)

	checksum := ComputeMd5ToHex([]byte(ts + task.Mid + task.OcrContent + SessionKey))

	params := fmt.Sprintf("ts=%s&id=%s&data=%s&checksum=%s", ts, task.Mid, url.QueryEscape(task.OcrContent), checksum)

	result, err := client.HttpPost(sysinfo.Url, params)
	if err != nil {
		seelog.Error("上传OCR信息失败：sysid=", sysinfo.Sysid)
		return
	}

	resp := &UploadResponse{}
	err = json.Unmarshal(result, resp)
	if err != nil {
		seelog.Errorf("解析OCR上传返回结果(%s)失败: %s", string(result), err.Error())
		return
	}

	if resp.Error == 0 {
		seelog.Errorf("上传OCR信息成功， filehash = %s, sysid = %s", task.FileHash, task.Sysid)
		DeleteTask(task)
	} else {
		seelog.Errorf("上传OCR信息失败， filehash = %s, sysid = %s, response = %s", task.FileHash, task.Sysid, string(result))
	}

}

func UploadOcrInfo() {
	SessionKey = GetConfigString("ocr", "SeesionKey", "")
	client := &HttpClientEx{}

	for {
		tasks := GetTasks(1000, 2)
		if len(tasks) == 0 {
			seelog.Info("未取到需要上报的数据，稍后再试...")
			time.Sleep(30 * time.Second)
			continue
		}

		for i, _ := range tasks {
			UploadData(tasks[i], client)
		}

		time.Sleep(5 * time.Second)
	}
}

func StartProcess() {
	go ProcessCloudOcr()
	go UploadOcrInfo()
}
